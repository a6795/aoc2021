import argparse
import sys
import statistics

_debug = False


def read_input(filename):
    with open(filename, 'r') as f:
        contents = f.readlines()
    return contents

def puzzle1(contents):
    
    positions = [int(position) for position in contents[0].split(',')]
    median_pos = int(statistics.median(positions))
    fuel_diff = [ abs(position - median_pos) for position in positions ]
    if _debug: print("Fuel spent:", sum(fuel_diff))
    return sum(fuel_diff)

def puzzle2(contents):
    positions = [int(position) for position in contents[0].split(',')]
    if _debug: print(statistics.mean(positions))
    mean = statistics.mean(positions)
    ## I'm not sure where the off by one error comes
    ## So this might be complete BS
    if mean % 1 < 0.75:
        mean = round(mean) - 1
    else:
        mean = round(mean)
    sum_n = lambda n: n*(n+1)/2
    fuel_diff = [ sum_n(abs(position - mean)) for position in positions ]
    if _debug: print("Fuel spent:", sum(fuel_diff))
    return int(sum(fuel_diff))


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='AOC 2021', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-p", type=int, default=1, help="Puzzle (1 or 2)")
    parser.add_argument("-d", action='store_true', help="enable debug")
    parser.add_argument("-t", action='store_true', help="test file")
    args = parser.parse_args()
    if args.d:
        _debug = True

    if args.t:
        contents = read_input('test_input')
    else:
        contents = read_input('input')
    
    if args.p == 1:
        print("Puzzle 1:", puzzle1(contents))
    elif args.p == 2:
        print("Puzzle 2:", puzzle2(contents))
    else:
        print("Puzzle number wrong. Please choose 1 or 2")
        sys.exit(1)
