import argparse
import sys
import re
import numpy as np

_debug = False


def read_input(filename):
    with open(filename, 'r') as f:
        contents = f.readlines()
    return contents

def puzzle1(contents):
    coords = []
    max_col, max_row = 0, 0
    for line in contents:
        re_groups = re.findall(f'(\d+),(\d+) -> (\d+),(\d+)', line)[0]
        ## in the problem description:
        ## X is the column
        ## Y is the row 
        ## make coords such that they indicate (row, column)
        coords.append(((int(re_groups[1]), int(re_groups[0])), (int(re_groups[3]), int(re_groups[2]))))
        local_max_col = max(int(re_groups[0]), int(re_groups[2]))
        if local_max_col > max_col:
            max_col = local_max_col
        
        local_max_row = max(int(re_groups[1]), int(re_groups[3]))
        if local_max_row > max_row:
            max_row = local_max_row
    if _debug: print("Map shape:", max_row+1, max_col+1)
    vents_map = np.zeros(shape=(max_row+1, max_col+1), dtype=int)
    
    ## in the problem description:
    ## X is the column
    ## Y is the row   
    ## in coords it's (row, column)
    to_remove = []
    for coord in coords:
        if _debug: print("Coord:", coord)
        if coord[0][0] == coord[1][0]:
            ## columns are the same (Xes)
            if _debug: print("Same row")
            _from = min(coord[0][1], coord[1][1])
            _to = max((coord[0][1], coord[1][1])) + 1
            if _debug: print("From", _from, "To", _to)
            for _i in range(_from, _to):
                if _debug: print("Updating", coord[0][0], _i)
                vents_map[coord[0][0], _i] += 1
            to_remove.append(coord)
        elif coord[0][1] == coord[1][1]:
            ## rows are the same (Ys)
            if _debug: print("Same  column")            
            _from = min(coord[0][0], coord[1][0])
            _to = max((coord[0][0], coord[1][0])) + 1
            if _debug: print("From", _from, "To", _to)
            for _i in range(_from, _to):
                if _debug: print("Updating", _i, coord[0][1] )
                vents_map[_i, coord[0][1]] += 1
            to_remove.append(coord)
            
    for _coord in to_remove:
        coords.remove(_coord)
    
    if _debug: print(vents_map)
    counts = np.where(vents_map > 1)
    if _debug: print("Counts:", len(counts[0]))
    return (len(counts[0]), vents_map, coords)

def puzzle2(contents):
    _counts, vents_map, coords = puzzle1(contents)
    if _debug: print("Dealing w/ diagonals:")
    ## in the problem description:
    ## X is the column
    ## Y is the row   
    ## in coords it's (row, column) 
    for coord in coords:
        if _debug: print("Coord:", coord)
        ## only have diagonals left to do
        if (abs(coord[0][0] - coord[1][0]) == abs(coord[0][1] - coord[1][1])):
            if _debug: print("Diagonal", coord)
            _start = coord[0] if coord[0] < coord[1] else coord[1]
            _end = coord[0] if coord[0] > coord[1] else coord[1]
            _diff = abs(coord[0][0] - coord[1][0]) + 1
            _right = -1 if _start[1] > _end[1] else 1
            if _debug: 
                print("starting coord:", _start)            
                print("Goes right", _right)
            for _i in range(_diff):
                    vents_map[_start[0] + _i, _start[1] + _i * _right] += 1
   
        
    if _debug: print(vents_map)
    counts = np.where(vents_map > 1)
    if _debug: print("Counts:", len(counts[0]))
    return len(counts[0])


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='AOC 2021', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-p", type=int, default=1, help="Puzzle (1 or 2)")
    parser.add_argument("-d", action='store_true', help="enable debug")
    parser.add_argument("-t", action='store_true', help="test file")
    args = parser.parse_args()
    if args.d:
        _debug = True

    if args.t:
        contents = read_input('test_input')
    else:
        contents = read_input('input')
    
    if args.p == 1:
        print("Puzzle 1:", puzzle1(contents)[0])
    elif args.p == 2:
        print("Puzzle 2:", puzzle2(contents))
    else:
        print("Puzzle number wrong. Please choose 1 or 2")
        sys.exit(1)
