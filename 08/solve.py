import argparse
import sys 

_debug = False


def read_input(filename):
    with open(filename, 'r') as f:
        contents = f.readlines()
    return contents

def puzzle1(contents):
    ## 1 = len 2, 4 = len 4, 7 = len 3, 8 = len 7
    uniques = [2, 4, 3, 7]
    signals = []
    output_digits = []
    for _i in range(len(contents)):
        _signals_input, _digits_input = contents[_i].split(' | ')
        signals.append(_signals_input.split(' '))
        output_digits.append(_digits_input.rstrip().split(' '))
    
    if _debug: print(output_digits)
    
    unique_segments = 0
    for output in output_digits:
        for digit in output:
            if len(digit) in uniques:
                unique_segments += 1
    
    if _debug: print("Digits with unique segments:", unique_segments)
    
    return unique_segments

def puzzle2(contents):
    ## 1 = len 2, 4 = len 4, 7 = len 3, 8 = len 7
    uniques = {2: 1, 4: 4, 3: 7, 7: 8}
    
    displays_sum = 0
    
    for _i in range(len(contents)):
        to_remove = []
        _signals_input, _digits_input = contents[_i].split(' | ')
        signals = _signals_input.split(' ')
        output_digits = _digits_input.rstrip().split(' ')
        signals_sets = [set(signal) for signal in signals]
        outputs_sets = [set(digit) for digit in output_digits]
        
        pattern = ['' for _ in range (10)]
        pattern_sets = [set() for _ in range(10)]
        
        for digit in signals:
            ## find the uniques
            if len(digit) in uniques:
                pattern[uniques[len(digit)]] = digit
                pattern_sets[uniques[len(digit)]] = set(digit)
                to_remove.append(digit)                
                
        if _debug: print("Uniques:", pattern)
        if _debug: print("To remove:", to_remove)
        for digit in to_remove:
            signals.remove(digit)
            signals_sets.remove(set(digit))
        
        ## find 6
        for _i in range(len(signals_sets)):
            if not pattern_sets[1].intersection(signals_sets[_i]) == pattern_sets[1] and len(signals_sets[_i]) == 6:
                pattern[6] = signals[_i]
                pattern_sets[6] = signals_sets[_i]
                
        signals.remove(pattern[6])
        signals_sets.remove(pattern_sets[6])
            
        if _debug: print("6:", pattern[6], pattern_sets[6])
        
        ## find 9
        for _i in range(len(signals_sets)):
            if pattern_sets[4].intersection(signals_sets[_i]) == pattern_sets[4] and len(signals_sets[_i]) == 6:
                pattern[9] = signals[_i]
                pattern_sets[9] = signals_sets[_i]
                
        signals.remove(pattern[9])
        signals_sets.remove(pattern_sets[9])
        
        if _debug: print("9:", pattern[9], pattern_sets[9])
    
        ## find 0 and 3
        for _i in range(len(signals_sets)):
            if pattern_sets[7].intersection(signals_sets[_i]) == pattern_sets[7]:
                if len(signals_sets[_i]) == 6:
                    pattern[0] = signals[_i]
                    pattern_sets[0] = signals_sets[_i]
                elif len(signals_sets[_i]) == 5:
                    pattern[3] = signals[_i]
                    pattern_sets[3] = signals_sets[_i]
                
        signals.remove(pattern[0])
        signals_sets.remove(pattern_sets[0])
        signals.remove(pattern[3])
        signals_sets.remove(pattern_sets[3])
        
        if _debug: print("0:", pattern[0], pattern_sets[0])
        if _debug: print("3:", pattern[3], pattern_sets[3])
        
        ## find segment c -- diff between 8 and 6
        segment_c = pattern_sets[8].difference(pattern_sets[6])
        if _debug: print("Segment c:", segment_c)
        
        ## find 5 and 2
        for _i in range(len(signals_sets)):
            ## segment c appears in 5 but not in 2
            if segment_c.intersection(signals_sets[_i]) == segment_c:
                pattern[2] = signals[_i]
                pattern_sets[2] = signals_sets[_i]
            else:
                pattern[5] = signals[_i]
                pattern_sets[5] = signals_sets[_i]
                
        if _debug: print("5:", pattern[5], pattern_sets[5])
        if _debug: print("2:", pattern[2], pattern_sets[2])
        
        if _debug: print("All patterns:", pattern)
        
        display = 0
        for _i in range(len(outputs_sets)):
            for _j in range(len(pattern_sets)):
                if outputs_sets[_i] == pattern_sets[_j]: 
                    ## match
                    display += _j * pow(10, len(output_digits) - 1 - _i)
        if _debug: print("Display:", display)
        displays_sum += display
    
    if _debug: print("Output sum:", displays_sum)
    return displays_sum                


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='AOC 2021', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-p", type=int, default=1, help="Puzzle (1 or 2)")
    parser.add_argument("-d", action='store_true', help="enable debug")
    parser.add_argument("-t", action='store_true', help="test file")
    args = parser.parse_args()
    if args.d:
        _debug = True

    if args.t:
        contents = read_input('test_input')
    else:
        contents = read_input('input')
    
    if args.p == 1:
        print("Puzzle 1:", puzzle1(contents))
    elif args.p == 2:
        print("Puzzle 2:", puzzle2(contents))
    else:
        print("Puzzle number wrong. Please choose 1 or 2")
        sys.exit(1)
