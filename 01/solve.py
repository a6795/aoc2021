import argparse
import sys

_debug = False


def read_input(filename):
    with open(filename, 'r') as f:
        contents = f.readlines()
    return contents

def puzzle1(contents):
    tokens = [int(t) for t in contents]
    if _debug: print("Input:", tokens)
    
    cnt = 0
    for _i in range(1, len(tokens)):
        if tokens[_i-1] < tokens[_i]:
            cnt +=1
    if _debug: print("Count:", cnt)
            
    return cnt

def puzzle2(contents):
    tokens = [int(t) for t in contents]
    
    cummulative_depths = [ sum(tokens[i:i+3]) for i in range(len(tokens)) ]
    if _debug: print("Cummulative depths:", cummulative_depths)
    return puzzle1(cummulative_depths)


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='AOC 2021', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-p", type=int, default=1, help="Puzzle (1 or 2)")
    parser.add_argument("-d", action='store_true', help="enable debug")
    parser.add_argument("-t", action='store_true', help="test file")
    args = parser.parse_args()
    if args.d:
        _debug = True

    if args.t:
        contents = read_input('test_input')
    else:
        contents = read_input('input')
    
    if args.p == 1:
        print("Puzzle 1:", puzzle1(contents))
    elif args.p == 2:
        print("Puzzle 2:", puzzle2(contents))
    else:
        print("Puzzle number wrong. Please choose 1 or 2")
        sys.exit(1)
