import argparse
import sys
import numpy as np

_debug = False
board_shape=5

class Board:
    def __init__(self, board_str):
        self.board = np.ndarray(shape=(board_shape,board_shape),dtype=int,buffer=np.fromstring(board_str, dtype=int, sep=' '))
        self.row_sum = [sum(self.board[_i,:]) for _i in range(board_shape)]
        self.col_sum = [sum(self.board[:,_i]) for _i in range(board_shape)]
        
    def update(self, val):
        if val in self.board:
            idxes = np.where(self.board == val)
            if _debug: print("Found", idxes)
            for _i in range(len(idxes[0])):
                if _debug: print("Updating", idxes[0][_i], idxes[1][_i])
                self.board[idxes[0][_i],idxes[1][_i]] = -1
            ## this is not efficient but I'm lazy
            self.row_sum = [sum(self.board[_i,:]) for _i in range(board_shape)]
            self.col_sum = [sum(self.board[:,_i]) for _i in range(board_shape)]
            if -board_shape in self.row_sum or -board_shape in self.col_sum:
                return True
            else:
                return False
        else:
            return False
    
    def print(self):
        print("Board:")
        print(self.board)
        print("Sum of rows:", self.row_sum)
        print("Sum of cols:", self.col_sum)

def read_input(filename):
    with open(filename, 'r') as f:
        contents = f.readlines()
    return contents

def puzzle1(contents):
    draw = np.fromstring(contents[0], dtype=int, sep=',')
    boards = []
    for _i in range(2, len(contents), board_shape+1):
        _board = ''
        for _j in range(board_shape):
            _board += contents[_i + _j].rstrip() + ' '
        boards.append(Board(_board))
    
    for val in draw:
        _found = False
        if _debug: print("Drawing:", val)
        for _board in boards:
            if _board.update(val):
                if _debug: _board.print()
                board_score = (np.sum(_board.board) + np.count_nonzero(_board.board == -1)) * val
                if _debug: print("Score:", board_score)
                _found = True
                break
        if _found:
            break
    
    return board_score

def puzzle2(contents):
    draw = np.fromstring(contents[0], dtype=int, sep=',')
    boards = []
    
    for _i in range(2, len(contents), board_shape+1):
        _board = ''
        for _j in range(board_shape):
            _board += contents[_i + _j].rstrip() + ' '
        boards.append(Board(_board))
    
    completed_boards = [False for _ in range(len(boards))]
    
    for val in draw:
        _found = False
        if _debug: print("Drawing:", val)
        for _i in range(len(boards)):
            _board = boards[_i]
            if _board.update(val):
                completed_boards[_i] = True
                if not False in completed_boards:
                    ## it was the last board to win
                    if _debug: _board.print()
                    board_score = (np.sum(_board.board) + np.count_nonzero(_board.board == -1)) * val
                    if _debug: print("Score:", board_score)
                    _found = True
                    break
        if _found:
            break
    
    return board_score


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='AOC 2021', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-p", type=int, default=1, help="Puzzle (1 or 2)")
    parser.add_argument("-d", action='store_true', help="enable debug")
    parser.add_argument("-t", action='store_true', help="test file")
    args = parser.parse_args()
    if args.d:
        _debug = True

    if args.t:
        contents = read_input('test_input')
    else:
        contents = read_input('input')
    
    if args.p == 1:
        print("Puzzle 1:", puzzle1(contents))
    elif args.p == 2:
        print("Puzzle 2:", puzzle2(contents))
    else:
        print("Puzzle number wrong. Please choose 1 or 2")
        sys.exit(1)
