import argparse
import sys
from collections import Counter

_debug = False


def read_input(filename):
    with open(filename, 'r') as f:
        contents = f.readlines()
    
    _contents = [c.rstrip() for c in contents]
    return _contents

def puzzle1(contents):
    tokens = [int(t) for t in contents]
    len_val = len(contents[0])
    
    gamma = []
    epsilon = []
    for _i in range(len_val):
        gamma_bits_slice = [_c[_i] for _c in contents]
        epsilon_bits_slice = [_c[len_val-1-_i] for _c in contents ]
        if _debug: print(f"{_i:>3} slice gamma:", gamma_bits_slice, f"{_i:>3} slice epsilon:", epsilon_bits_slice)
        bits_counters = (Counter(gamma_bits_slice)).most_common()
        gamma.append(bits_counters[0][0])
        epsilon.append(bits_counters[-1][0])
    if _debug: print("Gamma:", ''.join(gamma), "Epsilon:", ''.join(epsilon))
    return int(''.join(gamma), 2) * int(''.join(epsilon), 2)


def puzzle2(contents):
    len_val = len(contents[0])
    
    def filter_contents(least=True):
        _contents = list(contents)
        for _i in range(len_val):
            bits_slice = [_c[_i] for _c in _contents]
            bits_count = (Counter(bits_slice)).most_common(2)
            if bits_count[0][1] == bits_count[1][1]:
                bits_count = [('1', 0),('0',0)]
            _contents = [_c for _c in _contents if _c[_i] == bits_count[int(least)][0]]

            if _debug: print(f'Filtered {_i:>3}:', _contents)
            if len(_contents) == 1:
                return int(_contents[0], 2)
        
    if _debug: print('Oxigen:')
    oxigen_gen_rating = filter_contents(least=False)
    if _debug: print('CO2:')
    co2_scrubber_rating = filter_contents()

    if _debug: print(f'Oxigen generator rating: {oxigen_gen_rating}, CO2 scrubber rating: {co2_scrubber_rating}')
    
    return oxigen_gen_rating * co2_scrubber_rating


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='AOC 2021', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-p", type=int, default=1, help="Puzzle (1 or 2)")
    parser.add_argument("-d", action='store_true', help="enable debug")
    parser.add_argument("-t", action='store_true', help="test file")
    args = parser.parse_args()
    if args.d:
        _debug = True

    if args.t:
        contents = read_input('test_input')
    else:
        contents = read_input('input')
    
    if args.p == 1:
        print("Puzzle 1:", puzzle1(contents))
    elif args.p == 2:
        print("Puzzle 2:", puzzle2(contents))
    else:
        print("Puzzle number wrong. Please choose 1 or 2")
        sys.exit(1)
