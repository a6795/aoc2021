import argparse
import sys 

_debug = False


def read_input(filename):
    with open(filename, 'r') as f:
        contents = f.readlines()
    return contents

def puzzle1(contents, days):
    ## max 8 days
    ## count how many you have per day
    ## internal timer is the key
    
    fish = { }
    for _ in range(9):
        fish[_] = 0
        
    for timer in contents[0].split(','):
        fish[int(timer)] += 1
        
    for _day in range(days):
        for key, val in fish.items():
            if key == 0:
                ## new spawns
                new_fish = val
            else:
                fish[key - 1] = val
        fish[8] = new_fish
        fish[6] += new_fish
    
    total_fish = 0
    for key, val in fish.items():
        total_fish += val
    
    if _debug: print("Total fish:", total_fish)
    
    return total_fish

def puzzle2(contents):
    return puzzle1(contents, 256)


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='AOC 2021', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-p", type=int, default=1, help="Puzzle (1 or 2)")
    parser.add_argument("-d", action='store_true', help="enable debug")
    parser.add_argument("-t", action='store_true', help="test file")
    args = parser.parse_args()
    if args.d:
        _debug = True

    if args.t:
        contents = read_input('test_input')
    else:
        contents = read_input('input')
    
    if args.p == 1:
        print("Puzzle 1:", puzzle1(contents, 80))
    elif args.p == 2:
        print("Puzzle 2:", puzzle2(contents))
    else:
        print("Puzzle number wrong. Please choose 1 or 2")
        sys.exit(1)
