import argparse
import sys 

_debug = False


def read_input(filename):
    with open(filename, 'r') as f:
        contents = f.readlines()
    return contents


def puzzle1(contents):
    def forward(horiz, depth, val):
        horiz += val
        return horiz, depth

    def down(horiz, depth, val):
        depth += val
        return horiz, depth

    def up(horiz, depth, val):
        depth -= val
        return horiz, depth
    
    horiz, depth = 0, 0
    movement = {'forward': forward, 'down': down, 'up': up}
    
    for token in contents:
        move, val = token.split(' ')
        horiz, depth = movement[move](horiz, depth, int(val))
        if _debug: print("Movement:", move.ljust(10), "Horizontal:", horiz, "Depth:", depth, "Aim:", aim)
    
    if _debug: print("Horizontal:", horiz, "Depth:", depth)
    return horiz  * depth

def puzzle2(contents):
    def forward(horiz, depth, aim, val):
        horiz += val
        depth += aim*val
        return horiz, depth, aim

    def down(horiz, depth, aim, val):
        aim += val
        return horiz, depth, aim

    def up(horiz, depth, aim, val):
        aim -= val
        return horiz, depth, aim    
    
    horiz, depth, aim = 0, 0, 0
    movement = {'forward': forward, 'down': down, 'up': up}
    
    for token in contents:
        move, val = token.split(' ')
        horiz, depth, aim = movement[move](horiz, depth, aim, int(val))
        if _debug: print("Movement:", move.ljust(10), "Horizontal:", horiz, "Depth:", depth, "Aim:", aim)
    
    if _debug: print("Horizontal:", horiz, "Depth:", depth)
    return horiz  * depth


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='AOC 2021', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-p", type=int, default=1, help="Puzzle (1 or 2)")
    parser.add_argument("-d", action='store_true', help="enable debug")
    parser.add_argument("-t", action='store_true', help="test file")
    args = parser.parse_args()
    if args.d:
        _debug = True

    if args.t:
        contents = read_input('test_input')
    else:
        contents = read_input('input')
    
    if args.p == 1:
        print("Puzzle 1:", puzzle1(contents))
    elif args.p == 2:
        print("Puzzle 2:", puzzle2(contents))
    else:
        print("Puzzle number wrong. Please choose 1 or 2")
        sys.exit(1)
